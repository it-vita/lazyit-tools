import test from "./test";
import LzTimePicker from "./LzTimePicker";
import LzDatePicker from "./LzDatePicker";
import LzRangePicker from "./LzRangePicker";
import LzUpload from "./LzUpload";
import LzTinymce from "./LzTinymce";
import LzTable from "./LzTable";
import LzText from "./LzText";
import LzImage from "./LzImage";
import LzSearch from "./LzSearch";
import LzTree from "./LzTree";
import LzUploadTools from "./LzUploadTools";
import LzVideo from "./LzVideo";
import LzMonthPicker from "./LzMonthPicker";
import LzMultipleSelector from "./LzMultipleSelector";
import LzTreeSelect from "./LzTreeSelect";
import LzReport from "./LzReport";
import LzAmapChooseAddress from "./LzAmapChooseAddress";
import LzOneSelector from "./LzOneSelector";
import LzExportTools from "./LzExportTools";

const components = [
    test,
    LzTimePicker,
    LzDatePicker,
    LzRangePicker,
    LzUpload,
    LzTinymce,
    LzTable,
    LzText,
    LzImage,
    LzSearch,
    LzTree,
    LzUploadTools,
    LzVideo,
    LzMonthPicker,
    LzMultipleSelector,
    LzTreeSelect,
    LzReport,
    LzAmapChooseAddress,
    LzOneSelector,
    LzExportTools,
];
import "./global.less";
import Viewer from "v-viewer";
import "viewerjs/dist/viewer.css";

Viewer.setDefaults({
    Options: {
        inline: true,
        button: true,
        navbar: true,
        title: true,
        toolbar: true,
        tooltip: true,
        movable: true,
        zoomable: true,
        rotatable: true,
        scalable: true,
        transition: true,
        fullscreen: true,
        keyboard: true,
        url: "src",
    },
});

import VideoPlayer from "vue-video-player";

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = function(Vue) {
    // 判断是否安装
    if (install.installed) return;
    Vue.use(Viewer);
    Vue.use(VideoPlayer);

    // 遍历注册全局组件
    components.map((component) => Vue.component(component.name, component));
};

// 判断是否是直接引入文件
if (typeof window !== "undefined" && window.Vue) {
    install(window.Vue);
}

export default {
    // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
    install,
    // 以下是具体的组件列表
    ...components,
};
