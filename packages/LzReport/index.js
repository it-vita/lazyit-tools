import LzReport from './LzReport'

LzReport.install = function (Vue) {
    Vue.component(LzReport.name, LzReport)
}
export default LzReport
