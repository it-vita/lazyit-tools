export default [
    // {
    //     type: 'tree-select',
    //     dataIndex: 'ts',
    //     label: '树形下拉',
    //     defaultValue: '',
    //     allowLv: '>=0',//允许选择等级条件===n,!==n ,>=n ,<=n,>n,<n,false不使用条件
    //     asyncLoad:false,
    //     optionsConfig: {// {label,value,lv,[disabled]}
    //         url: 'http://localhost:7001/center/pc/serve/goods/category/getTree',
    //         label: 'cateName',
    //         value: 'id',
    //         lv: 'lv',
    //     }
    // },
    {
        type: 'text',
        dataIndex: 'title',
        label: '标题',
        defaultValue: ''
    },
    {
        type: 'select',
        dataIndex: 'state',
        label: '是否可见',
        defaultValue: '',
        options: [
            {
                label: '是',
                value: 1
            },
            {
                label: '否',
                value: 0
            }
        ]
    },
    // {
    //     type: 'select',
    //     dataIndex: 'classify',
    //     label: '类目',
    //     defaultValue: '',
    //     options: [],
    //     optionsConfig: {
    //         url: 'http://localhost:8102/base/classify/getAll',
    //         label: 'title',
    //         value: 'id'
    //     }
    // },
    {
        type: 'range-picker',
        dataIndex: 'dates',
        label: '日期范围',
        defaultValue: ''
    },
    // {
    //     type: 'month-picker',
    //     dataIndex: 'month',
    //     label: '月份选择',
    //     defaultValue: ''
    // }
]
