import LzUpload from './LzUpload'

LzUpload.install = function (Vue) {
    Vue.component(LzUpload.name, LzUpload)
}
export default LzUpload
