import test from './test'

test.install = function (Vue) {
    Vue.component(test.name, test)
}
export default test
