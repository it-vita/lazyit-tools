# 示例

> template

```vue
 <lz-table ref="myTable">
   <!--工具条-->
   <div slot="tools">
     <a-button icon="plus" @click="showAdd" type="primary">
     	添加
     </a-button>
     <a-button icon="delete" @click="dels" style="margin-left: 10px;" type="danger">
     	批量删除
     </a-button>
     <a-button icon="import" @click="imports" style="margin-left: 10px;" type="default">
     	导入
     </a-button>
     <a-button icon="export" @click="exports" style="margin-left: 10px;" type="default">
     	导出
     </a-button>
   </div>
 </lz-table>
```

> script 
>
> init方法应放到 mounted 中进行初始化

```javascript
this.$refs.myTable.init({
      url: globalConf.baseURL + '/base/label/getList', // 请求url
      columns: [{
        field: 'title',
        title: '标签名称',
        sorter: 'true',
        align: 'left'
      }],
      operate: {
        width: 200,
        items: [
          {
            show: true,
            label: '编辑',
            color: 'success',
            event: row => {
              this.$refs.edit.showModal(row.id)
            }
          },
          {
            show: true,
            label: '删除',
            color: 'danger',
            event: row => {
              this.$confirm({
                title: '确认删除？',
                content: '删除后将无法恢复！',
                onOk: () => {
                  this.$axios({
                    url: globalConf.baseURL + '/base/label/del',
                    method: 'post',
                    data: {
                      id: row.id
                    }
                  }).then(() => {
                    this.$message.success('删除成功')
                    this.$refs.myTable.selectedRowKeys = []
                    this.search()
                  })
                }
              })
            }
          }
        ]
      }
    }, () => {
      this.search()
    })
```

> init的更多属性-下面展示的为默认值

```javascript
method: 'get', // 请求方式（*）
pagination: true, // 是否显示分页（*）
queryParams: {}, // 默认参数
pageNumber: 1, // 初始化加载第一页，默认第一页
pageSize: 10, // 每页的记录行数（*）
pageList: [10, 25, 50, 100], // 可供选择的每页的行数（*）
showColumns: true, // 是否显示所有的列
showRefresh: true, // 是否显示刷新按钮
changeSize: true, // 允许改变列大小
showCheckBox: true, // 显示复选框
checkBoxProps: function(row) { // 设置复选框禁用 / 默认 选中状态
    return { 
        disabled: row.state !== 1, 
        checked: row.selected === 1
    }
},
showIndex: true, // 显示序号
height: 500, // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
```

> Init-column 字段格式化显示

> 格式化文本： -- 常用于标题，点击查看详情
>
> ```javascript
> {
>   field: 'title',
>   title: '名称',
>   sorter: 'true',
>   align: 'left',
>   width: 100,
>   formatter: {
>     type: 'text', //格式化方式
>     format: row => {
>       return {
>           value: '启用', //显示文本（必填）
>           color: 'primary' // 显示颜色 'primary','danger','success','warning','info'
>           event:()=>{ // 点击触发事件
>             // ...
>           }
>        }
>     }
>   }
> }
> ```

> 格式化为 图片 
>
> 用于显示单个图片 比如头像，商品主图
>
> ```javascript
> {
>   field: 'headImage',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'image', //格式化方式
>     format: row => {
>       return {
>       	name:'',// 名称
>       	url:''  // 图片路径
>       }
>     }
>   }
> }
> ```

> 格式化为图文
>
> 可用来显示 头像+名称效果，或者商品主图+名称+价格效果 
>
> ```javascript
> {
>   field: 'name',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'image-text', //格式化方式
>     format: row => {
>       return {
>       	name:'',// 名称
>       	url:''  // 图片路径
>       	text:[ //显示文本内容， 建议一行或两行
>           {
>             content:'显示文本'
>           },
>           {
>             content:'显示文本'
>           }
>       	]
>       }
>     }
>   }
> }
> ```

> 格式化为开关
>
> 常用于状态切换列，比如：是|否
>
> ```javascript
> {
>   field: 'state',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'image', //格式化方式
>     format: row => {
>       return {
>       	value:true,// 是否选中  true | false
>       	change:()=>{// change事件回调
>           const v = !row.state
>           // ...
>         }
>       }
>     }
>   }
> }
> ```

> 格式化徽章
>
> 常用于显示各种状态 如，等待审核-gray，审核通过-blue，审核拒绝-red
>
> ```javascript
> {
>   field: 'state',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'badge', //格式化方式
>     format: row => {
>       return {
>       	value:'审核通过',// 是否选中  true | false
>       	color:'blue'//徽章颜色'blue','green','gray','orange','yellow','red' 等
>       }
>     }
>   }
> }
> ```

lz-table 其他方法

```javascript
this.$refs.myTable.selectedRowKeys //获取或设置选中的行，[id,id,id,id]
```

```javascript
this.$refs.myTable.search(param || {}) // 执行搜索，param{k:v} json参数
```

