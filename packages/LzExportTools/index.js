import LzExportTools from "./LzExportTools";

LzExportTools.install = function(Vue) {
	Vue.component(LzExportTools.name, LzExportTools);
};
export default LzExportTools;
