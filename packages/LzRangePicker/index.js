import LzRangePicker from './LzRangePicker'

LzRangePicker.install = function (Vue) {
    Vue.component(LzRangePicker.name, LzRangePicker)
}
export default LzRangePicker
