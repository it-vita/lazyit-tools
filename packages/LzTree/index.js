import LzTree from './LzTree'

LzTree.install = function (Vue) {
    Vue.component(LzTree.name, LzTree)
}
export default LzTree
