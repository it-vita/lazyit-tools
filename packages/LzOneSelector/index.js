import LzOneSelector from "./LzOneSelector";

LzOneSelector.install = function(Vue) {
	Vue.component(LzOneSelector.name, LzOneSelector);
};
export default LzOneSelector;
