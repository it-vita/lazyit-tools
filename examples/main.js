import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
// 导入组件库
import lazyit from '../packages/index'
// 注册组件库
Vue.use(lazyit)
import {service} from './service.js'
Vue.prototype.$axios = service
Vue.prototype.$auth=()=>{return true}
import router from './router'
new Vue({
    render: h => h(App),
    router
}).$mount('#app')
