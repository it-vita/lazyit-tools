import LzDatePicker from './LzDatePicker'

LzDatePicker.install = function (Vue) {
    Vue.component(LzDatePicker.name, LzDatePicker)
}
export default LzDatePicker
