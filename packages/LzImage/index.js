import LzImage from './LzImage';

LzImage.install = function (Vue) {

    Vue.component(LzImage.name, LzImage);
};
export default LzImage;
