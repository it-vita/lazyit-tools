import LzTreeSelect from './LzTreeSelect'

LzTreeSelect.install = function (Vue) {
    Vue.component(LzTreeSelect.name, LzTreeSelect)
}
export default LzTreeSelect
