/* 首页*/
import index from "@/index";
/* 文件上传 */
import lzUpload from "@/lzUpload";
/* 搜索条 */
import search from "@/search";
/* 日期选择 */
import datePicker from "@/datePicker";
/* 日期范围选择 */
import rangePicker from "@/rangePicker";
/* 月选择 */
import monthPicker from "@/monthPicker";
/* 列表 */
import table from '@/table'
/* 树形 */
import lztree from '@/lztree'
/* 地图地址坐标选取 */
import mapChooseAddress from '@/mapChooseAddress'
/* 属性下拉 */
import treeSelect from '@/treeSelect'
/* 富文本 */
import tinymce from '@/tinymce'
export default [
  {
    id: "index",
    meta: { title: "欢迎" },
    name: "index",
    path: "/",
    component: index,
  },
  {
    id: "lzUpload",
    meta: { title: "文件上传" },
    name: "lzUpload",
    path: "/lzUpload",
    component: lzUpload,
  },
  {
    id: "search",
    meta: { title: "搜索条" },
    name: "search",
    path: "/search",
    component: search,
  },
  {
    id: "datePicker",
    meta: { title: "日期选择" },
    name: "datePicker",
    path: "/datePicker",
    component: datePicker,
  },
  {
    id: "rangePicker",
    meta: { title: "日期范围选择" },
    name: "rangePicker",
    path: "/rangePicker",
    component: rangePicker,
  },
  {
    id: "monthPicker",
    meta: { title: "月选择" },
    name: "monthPicker",
    path: "/monthPicker",
    component: monthPicker,
  },
  {
    id: "table",
    meta: { title: "列表" },
    name: "table",
    path: "/table",
    component: table,
  },
  {
    id: "lztree",
    meta: { title: "树形" },
    name: "lztree",
    path: "/lztree",
    component: lztree,
  },
  {
    id: "mapChooseAddress",
    meta: { title: "坐标拾取" },
    name: "mapChooseAddress",
    path: "/mapChooseAddress",
    component: mapChooseAddress,
  },
  {
    id: "treeSelect",
    meta: { title: "树形下拉" },
    name: "treeSelect",
    path: "/treeSelect",
    component: treeSelect,
  },{
    id: "tinymce",
    meta: { title: "富文本" },
    name: "tinymce",
    path: "/tinymce",
    component: tinymce,
  },
];
