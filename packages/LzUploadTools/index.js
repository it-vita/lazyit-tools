import LzUploadTools from './LzUploadTools'

LzUploadTools.install = function (Vue) {
    Vue.component(LzUploadTools.name, LzUploadTools)
}
export default LzUploadTools
