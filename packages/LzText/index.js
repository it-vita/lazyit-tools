import LzText from './LzText';

LzText.install = function (Vue) {

  Vue.component(LzText.name, LzText);
};
export default LzText;
