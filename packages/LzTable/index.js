import LzTable from './LzTable';

LzTable.install = function (Vue) {

  Vue.component(LzTable.name, LzTable);
};
export default LzTable;
