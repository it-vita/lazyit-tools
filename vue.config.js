const path = require("path");
const webpack = require('webpack')

const assetsCDN = {

    externals: {
        vue: 'Vue',
        'vue-router': 'VueRouter',
        vuex: 'Vuex',
        axios: 'axios',
        'ant-design-vue': 'ant-design-vue',
        moment: 'moment',
        "AMap": "AMap",
        'window.jQuery': 'jquery',
        '$': 'jquery'
    },
    css: [
        '//cdn.jsdelivr.net/npm/ant-design-vue@1.6.2/dist/antd.min.css'
    ],
    js: [
        '//cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.min.js',
        '//cdn.jsdelivr.net/npm/vue-router@3.1.3/dist/vue-router.min.js',
        '//cdn.jsdelivr.net/npm/vuex@3.1.1/dist/vuex.min.js',
        '//cdn.jsdelivr.net/npm/axios@0.19.0/dist/axios.min.js',
        '//momentjs.cn/downloads/moment.min.js',
        '//cdn.jsdelivr.net/npm/moment@2.27.0/locale/zh-cn.js',
        '//cdn.jsdelivr.net/npm/ant-design-vue@1.6.2/dist/antd.min.js',
        '//cdn.bootcss.com/jquery/1.12.4/jquery.min.js'
    ]
}

module.exports = {
    // disable source map in production
    productionSourceMap: false,
    lintOnSave: false,
    // babel-loader no-ignore node_modules/*
    transpileDependencies: [],

    // 修改 src 目录 为 examples 目录
    pages: {
        index: {
            entry: 'examples/main.js',
            template: 'public/index.html',
            filename: 'index.html',
            cdn: assetsCDN
        }
    },
    // 扩展 webpack 配置，
    chainWebpack: config => {
        //使 packages 加入编译
        config.module
            .rule('js')
            .include
            .add(__dirname + 'packages')
            .end()
            .use('babel')
            .loader('babel-loader')
            .tap(options => {
                // 修改它的选项...
                return options
            })
        // 解决ie11兼容ES6
        config.entry('main').add('babel-polyfill')
    },
    configureWebpack: {
        externals: assetsCDN.externals,
        resolve: {
            alias: {
                "@assets": path.resolve(__dirname, './packages/assets'),
                "@": path.resolve(__dirname, './examples/pages'),
                // 'jquery': path.resolve(__dirname, '../node_modules/jquery/src/jquery')
            }
        },
        plugins: [
            // Ignore all locale files of moment.js
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            new webpack.DefinePlugin({
                APP_VERSION: `"${require('./package.json').version}"`,
            })
        ],
    },
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    // less vars，customize ant design theme

                    // 'primary-color': '#F5222D',
                    // 'link-color': '#F5222D',
                    'border-radius-base': '2px'
                },
                // DO NOT REMOVE THIS LINE
                javascriptEnabled: true
            }
        }
    },
}
