import LzAmapChooseAddress from './LzAmapChooseAddress'

LzAmapChooseAddress.install = function (Vue) {
    Vue.component(LzAmapChooseAddress.name, LzAmapChooseAddress)
}
export default LzAmapChooseAddress
