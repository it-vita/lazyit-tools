import LzTimePicker from './LzTimePicker'

LzTimePicker.install = function (Vue) {
    Vue.component(LzTimePicker.name, LzTimePicker)
}
export default LzTimePicker
