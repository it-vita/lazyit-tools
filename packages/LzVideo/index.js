import LzVideo from './LzVideo';

LzVideo.install = function (Vue) {

    Vue.component(LzVideo.name, LzVideo);
};
export default LzVideo;
