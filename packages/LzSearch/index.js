import LzSearch from './LzSearch'

LzSearch.install = function (Vue) {
    Vue.component(LzSearch.name, LzSearch)
}
export default LzSearch
