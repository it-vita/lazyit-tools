import LzMultipleSelector from './LzMultipleSelector'

LzMultipleSelector.install = function (Vue) {
    Vue.component(LzMultipleSelector.name, LzMultipleSelector)
}
export default LzMultipleSelector
