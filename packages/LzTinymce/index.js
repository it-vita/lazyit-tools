import LzTinymce from './LzTinymce'

LzTinymce.install = function (Vue) {
    Vue.component(LzTinymce.name, LzTinymce)
}
export default LzTinymce
