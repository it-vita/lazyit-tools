# lz-table表格组件

## template

```vue
 <lz-table ref="myTable">
   <!--工具条-->
   <div slot="tools">
     <a-button icon="plus" @click="showAdd" type="primary">
     	添加
     </a-button>
     <a-button icon="delete" @click="dels" style="margin-left: 10px;" type="danger">
     	批量删除
     </a-button>
     <a-button icon="import" @click="imports" style="margin-left: 10px;" type="default">
     	导入
     </a-button>
     <a-button icon="export" @click="exports" style="margin-left: 10px;" type="default">
     	导出
     </a-button>
   </div>
 </lz-table>
```

## script 

> init方法应放到 mounted 中进行初始化

```javascript
this.$refs.myTable.init({
      url: globalConf.baseURL + '/base/label/getList', // 请求url
      columns: [{
        field: 'title',
        title: '标签名称',
        sorter: 'true',
        align: 'left'
      }],
      operate: {
        width: 200,
        items: [
          {
            label: '编辑',
            color: 'success',
            permission: 'table:edit',
            event: row => {
              this.$refs.edit.showModal(row.id)
            },
            //按钮隐藏条件
            hidden: row => {
              if(row.state===1){
                return true //隐藏
              }else{
                return false
              }
            }
          },
          {
            label: '删除',
            color: 'danger',
            permission: 'table:edit',
            event: row => {
              this.$confirm({
                title: '确认删除？',
                content: '删除后将无法恢复！',
                onOk: () => {
                  this.$axios({
                    url: globalConf.baseURL + '/base/label/del',
                    method: 'post',
                    data: {
                      id: row.id
                    }
                  }).then(() => {
                    this.$message.success('删除成功')
                    this.$refs.myTable.selectedRowKeys = []
                    this.search({})
                  })
                }
              })
            }
          }
        ]
      }
    }, () => {
      this.search({})
    })
```

## 属性

init的更多属性-下面展示的为默认值

| 属性         | 说明                                                         | 是否必填 | 类型     | 默认值            |
| ------------ | ------------------------------------------------------------ | -------- | -------- | ----------------- |
| url          | 请求链接                                                     | 是       | String   |                   |
| method       | 请求方式                                                     | 否       | post/get | get               |
| pagination   | 是否显示分页                                                 | 否       | boolean  | true              |
| queryParams  | 默认参数                                                     | 否       | Object   | {}                |
| pageNumber   | 初始化加载第一页，默认第一页                                 | 否       | Number   | 1                 |
| pageSize     | 每页的记录行数（*）                                          | 否       | Number   | 10                |
| pageList     | 可供选择的每页的行数                                         | 否       | Array    | [10, 25, 50, 100] |
| showColumns  | 是否显示所有的列                                             | 否       | boolean  | true              |
| showRefresh  | 是否显示刷新按钮                                             | 否       | boolean  | true              |
| changeSize   | 允许改变列大小                                               | 否       | boolean  | true              |
| showCheckBox | 显示复选框                                                   | 否       | boolean  | false             |
| showIndex    | 显示序号                                                     | 否       | boolean  | true              |
| height       | 行高，如果没有设置height属性，表格自动根据记录条数设置表格高度 | 否       | Number   | 500               |
| columns      | 定义列                                                       | 是       | Array    |                   |
| operate      | 定义操作                                                     | 否       | Array    |                   |

## columns示例

> ```
>[{
field: 'title', //字段名
>title: '标签名称', //显示名
> sorter: 'true', //是否可以排序
> permission: 'table:edit', //权限标识
> width:'200px',//宽度 ，不配置则自适应宽度
> align: 'left'//对齐方式 可选：left center right
> },
> {
> field: 'name',
> title: '名称',
> sorter: 'true',
> align: 'left'
> }]
> ```
> 
> columns字段格式化显示
> 
> 格式化文本： -- 常用于标题，点击查看详情
>
> ```javascript
>{
> field: 'title',
>title: '名称',
> sorter: 'true',
> align: 'left',
> width: 100,
> formatter: {
> type: 'text', //格式化方式
> format: row => {
>    return {
>        value: row.title, //显示文本（必填）
>          color: 'primary', // 显示颜色 'primary','danger','success','warning','info','default'
>          event:()=>{ // 点击触发事件
>             // ...
>           }
>        }
>    }
>    }
>    }
>    ```

> 格式化为 图片 
>
> 用于显示单个图片 比如头像，商品主图
>
> ```javascript
> {
>   field: 'headImage',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'image', //格式化方式
>     format: row => {
>       return {
>       	name:'',// 名称
>       	url:''  // 图片路径
>       }
>     }
>   }
> }
> ```

> 格式化为图文
>
> 可用来显示 头像+名称效果，或者商品主图+名称+价格效果 
>
> ```javascript
> {
>   field: 'name',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'image-text', //格式化方式
>     format: row => {
>       return {
>       	name:'',// 名称
>       	url:''  // 图片路径
>       	text:[ //显示文本内容， 建议一行或两行
>           {
>             content:'显示文本'
>           },
>           {
>             content:'显示文本'
>           }
>       	]
>       }
>     }
>   }
> }
> ```

> 格式化为开关
>
> 常用于状态切换列，比如：是|否
>
> ```javascript
> {
>   field: 'state',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'switch', //格式化方式
>     format: row => {
>       return {
>       	value:true,// 是否选中  true | false
>       	change:()=>{// change事件回调
>           const v = !row.state
>           // ...
>         }
>       }
>     }
>   }
> }
> ```

> 格式化徽章
>
> 常用于显示各种状态 如，等待审核-gray，审核通过-blue，审核拒绝-red
>
> ```javascript
> {
>   field: 'state',
>   title: '状态',
>   sorter: 'true',
>   align: 'center',
>   width: 100,
>   formatter: {
>     type: 'badge', //格式化方式
>     format: row => {
>       return {
>       	value:'审核通过',
>       	color:'blue'//徽章颜色'blue','green','gray','orange','yellow','red' 等
>       }
>     }
>   }
> }
> ```

## 方法

```javascript
this.$refs.myTable.selectedRowKeys //获取或设置选中的行，[id,id,id,id]
```

```java
this.$refs.myTable.selectedRows //获取或设置选中的行，[{row},{},{}]
```

```javascript
this.$refs.myTable.search(param || {}) // 执行搜索，param{k:v} json参数
```

```javascript
this.$refs.myTable.getSelected() //获取选中项 {selectedRowKeys,selectedRows}
```

```javascript
this.$refs.myTable.setSelected(selectedRowKeys,selectedRows)//设置选中项
```

# lz-search搜索组件

> 更新日志

- <font color='red'>2020-09-15：新增树形下拉懒加载数据</font>
- <font color='red'>2020-05-15：新增月份选择框</font>

> template

```vue
  <lz-search ref="searchBar" :items="items" @search="search"></lz-search>
```

> items说明

```javascript
export default [
//文本
  {
    type: 'text', //组件类型
    dataIndex: 'name', //组件key
    label: '名称', //组件显示名称
    defaultValue: '' //默认值
  },
  //下拉静态数据
  {
    type: 'select',
    dataIndex: 'state',
    label: '状态',
    defaultValue: '',
    options: [ //选项配置
      {
        label: '启用',//名称
        value: '1' //值
      },
      {
        label: '禁用',
        value: '0'
      }
    ]
  },
  //下拉动态数据
   {
    type: 'select',
    dataIndex: 'state',
    label: '状态',
    defaultValue: '',
    options: [],
    optionsConfig: { //动态数据配置
       url:'http://localhost:8102/base/classify/getAll',//数据请求地址result:{code:1,content:[]}
       label: 'title',// label对应key  不设置默认label
       value: 'id' //value 对应key  不设置默认value
    }
  },
    //树形下拉
   {
    type: 'tree-select',
    dataIndex: 'dept',
    label: '树形下拉',
    defaultValue: '',
    allowLv: '>=0',//允许选择层级的条件===n,!==n ,>=n ,<=n,>n,<n,false 不使用条件(不使用条件时，可通过返回结果内增加disabled=[true|false]控制是否可选)
    asyncLoad:false,//是否开启数据懒加载（开启后会在请求url上增加参数?pid=value）
    optionsConfig: {
       url:'http://localhost:8102/base/classify/getAll',//数据请求地址result:{code:1,content:[{label,value,lv,[disabled]}]}
       label: 'title',// label对应key  不设置默认title
       value: 'id', //value 对应key  不设置默认value
       lv: 'lv'//层级 不设置 默认 lv
    }
  },
  //日期范围
  {
    type: 'range-picker',
    dataIndex: 'dates',
    label: '日期范围',
    defaultValue: ''
  },
  //月选择框
  {
    type: 'month-picker',
    dataIndex: 'month',
    label: '月份选择',
    defaultValue: ''
  }
]
```

> 事件

| 事件名称 | 说明         | 回调参数         |
| -------- | ------------ | ---------------- |
| search   | 节点点击事件 | function(values) |

> values：搜索条件值

> 方法

| 事件名称  | 说明               | 使用示例                         |
| --------- | ------------------ | -------------------------------- |
| getValues | 主动获取搜索条件值 | this.$refs.searchBar.getValues() |

# lz-tree  树形

## template

```vue
<lz-tree ref="lzTree" :replaceFields="{
             key: 'key',
            title: 'title',
            children: 'children'
        }"
        :asyncLoad="asyncLoad"
        @click="nodeClick"></lz-tree>
```

*asyncLoad：异步加载数据，不需要异步时可不配置*

## script

```javascript
 this.$refs.lzTree.init(treeData)//treeData 为初始化数组，异步时treeData若不填，则默认请求一次asyncLoad({id:0})
```

## 属性说明

| 属性          | 说明                                       | 类型             | 默认值                                                       |
| ------------- | ------------------------------------------ | ---------------- | ------------------------------------------------------------ |
| replaceFields | 替换结果集中key，title，children为对应字段 | Object           | {<br/>       key: 'key',<br/>        title: 'title',<br/>         children: 'children'<br/>   } |
| asyncLoad     | 异步加载数据                               | function（node） | -                                                            |

> replaceFields示例

```javascript
{
    key: 'id',  // 唯一值 ，默认id
    title: 'name', // 显示内容，默认name
    children: 'children' //直接点键
  }
```

> asyncLoad实例

```javascript
asyncLoad({id}){
  return new Promise(resolve => {
    this.$axios({
      url:'http://localhost:7001/center/pc/serve/goods/category/getTree',
      params:{pid:id}
    }).then(res=>{
      resolve(res.data) //子节点数据 []
    })
  })
},
```

## 事件说明

| 事件名称 | 说明         | 回调参数        |
| -------- | ------------ | --------------- |
| click    | 节点点击事件 | function(value) |

> value:当前选中节点json数据

## 方法说明

| 方法名称     | 参数 | 说明             |
| ------------ | ---- | ---------------- |
| refreshByPid | id   | 属性某节点下数据 |

> 例：
>
> ```javascript
> this.$refs.lzTree.refreshByPid("1308741694174265344");//刷新id 1308741694174265344下的直接点数据
> ```

# lz-text文本预览

> template

```vue
<lz-text :value="lztext" color="primary" @click="textClick"></lz-text>
```

> 属性说明

| 属性  | 说明                                  | 类型   | 默认值 |
| ----- | ------------------------------------- | ------ | ------ |
| color | 颜色（primary,success,warning,error） | String |        |

> 事件

| 事件名称 | 说明                      | 回调参数        |
| -------- | ------------------------- | --------------- |
| click    | 点击事件，返回参数 lztext | function(value) |

> value说明：传入值lztext

# lz-upload-tools 文件上传-表格工具条

单文件上传，常用于table表格工具条

> template

```vue
<lz-upload-tools
        @beforeUpload="beforeUpload"
        @doneUpload="doneUpload"
        label="导入"
        :size="1"
        accept=".xlsx,.xls"
        action='http://localhost:8102/base/common/upload/image'
/>
```

> 属性

| 属性   | 说明                    | 类型   | 默认值 |
| ------ | ----------------------- | ------ | ------ |
| label  | 组件名称                | String | 导入   |
| size   | 允许上传文件大小 单位MB | Number | 2      |
| accept | 允许上传文件类型        | String |        |
| action | 文件上传路径            | String |        |

> 事件

| 事件名称     | 说明               | 回调参数         |
| ------------ | ------------------ | ---------------- |
| beforeUpload | 文件开始上传前调用 | function(values) |
| doneUpload   | 文件上传完成后调用 | function(values) |

> values示例

```javascript
{
		file:File,
		id: "1qgc04h0lsclr7bhtcby"
    name: "文件名.xlsx"
    original: "源文件路径"
    path: "压缩文件路径"
    state: 2 //状态，1开始上传，2上传成功，3上传失败
	}
```

# lz-upload文件上传-表单

多文件上传

## template

```vue
<lz-upload
        v-model="uploadList"
        :max-num="5"
        :size="1"
        @change="uploadChange"
        accept=".jpg,.png,.xlsx"
        :disabled="false"
        imgBasePath="https://echftp.yqzhfw.com/"
        action='http://localhost:8102/base/common/upload/image'
        :replaceFields="{
          name: 'name',
          path: 'path',
          original: 'original',
          size: 'size',
          suffix: 'suffix',
        }"
        :step="{ size: 1024 }"
/>
```

## 属性说明

| 属性           | 说明               | 类型    | 默认值                                                       |
| -------------- | ------------------ | ------- | ------------------------------------------------------------ |
| value(v-model) | 设置上传文件默认值 | Array   | []                                                           |
| max-num        | 允许最大文件数     | Number  | 0                                                            |
| size           | 单位MB             | Number  | 2                                                            |
| accept         | 允许上传文件类型   | String  | 几乎所有文件                                                 |
| disabled       | 是否禁用           | boolean | false                                                        |
| imgBasePath    | 文件访问跟路径     | String  |                                                              |
| action         | 文件上传路径       | String  |                                                              |
| replaceFields  | 结果 key替换       | Object  | {<br/>          name: 'name',<br/>          path: 'path',<br/>          original: 'original',<br/>          size: 'size',<br/>          suffix: 'suffix',<br/>  } |
| step           | 开启分片上传       | Object  | false                                                        |

### step说明

| 属性 | 说明             | 类型   | 默认值 |
| ---- | ---------------- | ------ | ------ |
| size | 分片大小，单位kb | Number | 无     |



## 事件

| 事件名称 | 说明               | 回调参数       |
| -------- | ------------------ | -------------- |
| change   | 文件发生改变后调用 | Change(values) |

### values示例

```
[{
        name: '音频服务.png',  //必须
        path: '/basis/2020/7/10/13acde2b08e84fc7bae4e9c0195facfc.png' //必须
	}]
```

# lz-image 图片预览

```vue
<lz-image url="path" :data-list="dataList"></lz-image>
```

> 属性

| 属性     | 说明                       | 类型   | 默认值 |
| -------- | -------------------------- | ------ | ------ |
| url      | dataList 中视频访问地址key | String | path   |
| dataList | 视频列表                   | Array  |        |
| baseUrl  | url跟地址                  | String |        |

> dataList

```json
[
  {
  	path: 'https://echftp.yqzhfw.com/xxx.png'
  },
  {
  	path: 'https://echftp.yqzhfw.com/xxx.png'
  },
  {
  	path: 'https://echftp.yqzhfw.com/xxx.png'
  }
]
```

# lz-video 视频预览

> template

```vue
<lz-video url="path" :data-list="videoDataList"></lz-video>
```

> 属性

| 属性     | 说明                       | 类型   | 默认值 |
| -------- | -------------------------- | ------ | ------ |
| url      | dataList 中视频访问地址key | String | path   |
| dataList | 视频列表                   | Array  |        |
| baseUrl  | url跟地址                  | String |        |

> dataList示例

```json
[
  {
  	path: 'https://echftp.yqzhfw.com/xxx.mp4'
  },
  {
  	path: 'https://echftp.yqzhfw.com/xxx.mp4
  },
  {
  	path: 'https://echftp.yqzhfw.com/xxx.mp4'
  }
]
```

# lz-tinymce 富文本

> template

```vue
<lz-tinymce
            v-model="a"
            height="300px"
            staticPath="https://ksource.yqzhfw.com/resource"
            action="https://ksource.yqzhfw.com/basis/api/base/v1/base/common/file/fileUpload"
            basePath="https://echftp.yqzhfw.com/"
  />
```

> 属性说明

| 属性           | 说明                             | 类型    | 默认值  |
| -------------- | -------------------------------- | ------- | ------- |
| value(v-model) | 用于设置当前富文本的内容         | String  |         |
| disabled       | 设置当前是否可编辑               | boolean | false   |
| skin           | 皮肤["light", "dark"]            | String  | light   |
| width          | 宽度                             | String  | 100%    |
| height         | 高度                             | String  | 300px   |
| staticPath     | 静态资源路径（可设为网络跟地址） | String  | /static |
| action         | 上传地址                         | String  |         |
| basePath       | 文件访问跟地址                   | String  |         |

> 事件

| 事件名称 | 说明           | 回调参数         |
| -------- | -------------- | ---------------- |
| input    | 输入时回调     | function(values) |
| change   | 内容变化时回调 | function(values) |

> values示例

```
<p>xxx</p>
```

# lz-multiple-selector多选选择器

> 效果图

![wg8yb6.jpg](https://s1.ax1x.com/2020/09/16/wg8yb6.jpg)

> template

```vue
<lz-multiple-selector
               action="http://localhost:7001/center/pc/serve/goods/category/getList"
               :replaceFields="{
                    title:'cateName,cateCode',
                    description:'createTime'
               }"
               v-model="ms"
               @change="msChange"
            />
```

> 属性说明

| 属性             | 说明                                                         | 类型    | 默认值                                                       |
| ---------------- | ------------------------------------------------------------ | ------- | ------------------------------------------------------------ |
| value(v-model)   | 用于设置已选内容                                             | Array   | []                                                           |
| action           | 左侧数据请求url<br>（自动附加请求参数：pageSize，pageNumber，keyword） | String  |                                                              |
| replaceFields    | 显示的内容可以用(,)分割在一行展示多列                        | Object  | {    <br >  title: 'title',   <br>  description: 'description'<br> } |
| show-description | 是否显示描述行                                               | Boolean | True                                                         |

> 事件

| 事件名称 | 说明               | 回调参数         |
| -------- | ------------------ | ---------------- |
| change   | 结果发生改变时调用 | function(values) |

> values示例

```javascript
[
  {
    id:'1',
    title:'title1',
    description:'description1'
  },
  {
    id:'2',
    title:'title2',
    description:'description2'
  }
]
```

# Lz-tree-select 下拉树选择

![whBNOP.jpg](https://s1.ax1x.com/2020/09/18/whBNOP.jpg)
![whBtyt.jpg](https://s1.ax1x.com/2020/09/18/whBtyt.jpg)

## template

```vue
<lz-tree-select
        ref="lzTreeSelect"
        :replaceFields="{
                key: 'id',
                value: 'id',
                title: 'cateName',
                children: 'children'
            }"
        v-model="treeSelect"
        @change="lzTreeSelectChange"
        :multiple="true"
        :asyncLoad="asyncLoad"
/>
```

*asyncLoad：异步加载数据，不需要异步时可不配置*

## script

```javascript
this.$refs.lzTreeSelect.init(treeData)//treeData 为初始化数组，异步时treeData若不填，则默认请求一次asyncLoad({id:0})
```

## 属性说明

| 属性           | 说明                                             | 类型             | 默认值                                                       |
| -------------- | ------------------------------------------------ | ---------------- | ------------------------------------------------------------ |
| replaceFields  | 替换结果集中key，value,title，children为对应字段 | Object           | {<br/>       key: 'key',<br/>        value:'value',<br>         title: 'title',<br/>         children: 'children'<br/>   } |
| v-model(value) | 绑定值单选时String ，多选时 Array                | String/Array     |                                                              |
| multiple       | 是否开启多选                                     | Boolean          | false                                                        |
| asyncLoad      | 异步加载数据                                     | function（node） | -                                                            |

> asyncLoad实例

```javascript
asyncLoad({id}) {
    return new Promise(resolve => {
        this.$axios({
            url: 'http://localhost:7001/center/pc/serve/goods/category/getTree',
            params: {pid: id}
        }).then(res => {
            resolve(res.data)
        })
    })
},
```

## 事件说明

| 事件名称 | 说明           | 回调参数        |
| -------- | -------------- | --------------- |
| change   | 选中值改变触发 | function(value) |

# lz-report报表展示

[![0SjV4x.jpg](https://s1.ax1x.com/2020/09/24/0SjV4x.jpg)](https://imgchr.com/i/0SjV4x)

> template

```vue
 <lz-report
          ref="lzReport"
          domain="http://localhost:7001"
          reportId="ff80808174b5c3c80174b88705050003"
          reportName="供货单"
        >
        <a-button @click="hideModal">关闭</a-button>
        </lz-report>
```

> 属性

| 属性       | 说明             | 类型   | 默认值    |
| ---------- | ---------------- | ------ | --------- |
| domain     | 报表服务请求域名 | String |           |
| reportId   | 报表ID           | String |           |
| reportName | 报表导出名称     | String | :reportId |

> 扩展

slot 内容将插入在顶部按钮后方，经常用于扩展一个页面关闭按钮

>Script - init初始化

```javascript
this.$refs.lzReport.init(param, () => {
  this.spinning = false
})
```

> 说明

| 属性     | 说明           | 类型       | 默认值 |
| -------- | -------------- | ---------- | ------ |
| param    | 报表请求参数   | String     |        |
| callback | 初始化成功回调 | function() |        |

# lz-amap-choose-address高德地图选点

[![09BRFx.md.jpg](https://s1.ax1x.com/2020/09/25/09BRFx.md.jpg)](https://imgchr.com/i/09BRFx)
[![09BWY6.md.jpg](https://s1.ax1x.com/2020/09/25/09BWY6.md.jpg)](https://imgchr.com/i/09BWY6)

## template

```vue
<lz-amap-choose-address
        map-key="87ff1e887140aa980a075c096a434940"
        placeholder="请设置地址"
        v-model="chooseArddr"
        @change="chooseArddrChange"/>
```

## 属性

| 属性            | 说明                  | 类型   | 默认值                                              |
| --------------- | --------------------- | ------ | --------------------------------------------------- |
| map-key         | 高德地图注册申请的key | String |                                                     |
| v-model(:value) | 绑定的值              | Object | {<br>    addr:''<br/>    lat:'',<br>    lng:''<br>} |
| placeholder     | 空提示                | String |                                                     |

## 事件

| 事件名称 | 说明           | 回调参数        |
| -------- | -------------- | --------------- |
| change   | 内容改变时触发 | function(value) |

## 示例

```vue
<template>
  <div>
    <lz-amap-choose-address
      map-key="87ff1e887140aa980a075c096a434940"
      placeholder="请设置地址"
      v-model="chooseArddr"
      @change="chooseArddrChange"
    />
    选择结果：{{chooseArddr}}
  </div>
</template>
<script>
export default {
  data() {
    return {
      chooseArddr: {
        addr: "河南省郑州市金水区文化路街道河南省农业科学院1",
        lat: "34.788996",
        lng: "113.679317",
      },
    };
  },
  created() {},
  mounted() {},
  methods: {
    chooseArddrChange(val) {
      console.log(val);
    },
  },
};
</script>
<style></style>

```



# lz-one-selector单选

1.[![0VAh8I.jpg](https://s1.ax1x.com/2020/09/28/0VAh8I.jpg)](https://imgchr.com/i/0VAh8I)
2.[![0VA5xP.md.jpg](https://s1.ax1x.com/2020/09/28/0VA5xP.md.jpg)](https://imgchr.com/i/0VA5xP)
3.[![0VA42t.jpg](https://s1.ax1x.com/2020/09/28/0VA42t.jpg)](https://imgchr.com/i/0VA42t)

> template

```vue
<lz-one-selector
                    action="http://localhost:7001/center/pc/serve/goods/category/getList"
                    :replaceFields="{
                        title: 'cateName,cateCode',
                        description: 'createTime',
                    }"
                    v-model="os"
                    @change="osChange"
                    :show-description="true"
                />
```

> 属性说明

| 属性             | 说明                                                         | 类型    | 默认值                                                       |
| ---------------- | ------------------------------------------------------------ | ------- | ------------------------------------------------------------ |
| value(v-model)   | 用于设置已选内容                                             | Array   | []                                                           |
| action           | 左侧数据请求url<br>（自动附加请求参数：pageSize，pageNumber，keyword） | String  |                                                              |
| replaceFields    | 显示的内容可以用(,)分割在一行展示多列                        | Object  | {    <br >  title: 'title',   <br>  description: 'description'<br> } |
| show-description | 是否显示描述行                                               | Boolean | True                                                         |

> 事件

| 事件名称 | 说明               | 回调参数         |
| -------- | ------------------ | ---------------- |
| change   | 结果发生改变时调用 | function(values) |

> values示例

```javascript
	{
    id:'1',
    title:'title1',
    description:'description1'
  }

```

# lz-export-tools 工具条报表导出

> Template

```vue
<lz-export-tools
        ref="lzExportTools"
        domain="http://localhost:7001"
        reportId="ff80808174d28d1b0174d43fb7e40001"
        reportName="测试导出"
         @click="exportClick" />
```

> 属性

| 属性       | 说明             | 类型   | 默认值    |
| ---------- | ---------------- | ------ | --------- |
| domain     | 报表服务请求域名 | String |           |
| reportId   | 报表ID           | String |           |
| reportName | 报表导出名称     | String | :reportId |

> 事件

| 事件名称 | 说明         | 回调参数 |
| -------- | ------------ | -------- |
| click    | 按钮点击事件 |          |

> 方法

| 方法名称    | 说明      | 参数格式 |
| ----------- | --------- | -------- |
| exportExcel | 导出excel | Object   |
| exportWord  | 导出word  | Object   |
| exportPdf   | 导出pdf   | Object   |

> 示例：

```javascript
exportClick(){
  this.$refs.lzExportTools.exportExcel(param)
},
```

> 示例说明

| 属性  | 说明         | 类型   | 默认值 |
| ----- | ------------ | ------ | ------ |
| param | 报表请求参数 | Object |        |