import axios from 'axios'

// 使用create方法创建axios实例
export const service = axios.create({
    timeout: 7000, // 请求超时时间
    baseURL: '',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    }
})
// 添加请求拦截器
service.interceptors.request.use(config => {
    // Object.assign(config.headers, headers())
    return config
})
// 添加响应拦截器
service.interceptors.response.use(response => {
    return response.data
}, error => {
    const msg = ((error.response || {}).data || {}).info || '请求出错'
    console.log(msg)
    return Promise.reject(error)
})
