import LzMonthPicker from './LzMonthPicker'

LzMonthPicker.install = function (Vue) {
    Vue.component(LzMonthPicker.name, LzMonthPicker)
}
export default LzMonthPicker
